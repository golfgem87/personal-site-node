var express = require('express'),
    mailer = require('express-mailer');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/images', express.static(__dirname + '/images'));

mailer.extend(app, {
  from: 'no-reply@garrettmark.net',
  host: 'smtp.gmail.com', // hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'gmark.uchs@gmail.com',
    pass: 'nikeone1'
  }
});

app.get('/', function (req, res) {
	res.sendFile(__dirname+'/views/index.html');
});

app.post('/', function (request, res) {
	var form = request.body;
	app.mailer.send('email', {
    to: 'golfgem87@yahoo.com', // REQUIRED. This can be a comma delimited string just like a normal email to field. 
    subject: 'garrettmark.net Contact!', // REQUIRED.
    otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables.
  }, function (err) {
    if (err) {
      // handle error
      console.log(err);
      res.send('There was an error sending the email');
      return;
    }
    res.send('Email Sent');
  });
});

app.listen((process.env.PORT), function () {
	console.log('App listening on port 5000!');
});