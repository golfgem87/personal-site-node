(function () {
  var app = angular.module('gm', []);

  app.filter('range', function() {
    return function(val, range) {
      range = parseInt(range, 10);
      for (var i = 0; i < range; i++) {
        val.push(i);
      }
      return val;
    };
  });

  app.controller('MainController', ['$scope', '$http', function($scope, $http){
    $scope.data = {};
    $scope.submit = function(mailForm) {
      console.log($scope.data);
      console.log(mailForm);
    // check to make sure the form is completely valid
      if (mailForm.$valid) {
        $http({
              method: 'POST', url: '/', data: $scope.data
            }).
              success(function(data, status, headers, config) {
                $window.location.replace('/');
        });
      }

    };

    $scope.today = new Date();
    $scope.resume = {};
    $scope.content = {};
    $http.get('./assets/json/resume.json').success(function(data) {
      $scope.resume = data;
    });
    $http.get('./assets/json/content.json').success(function(data) {
      $scope.content = data;
    });

  }]);

})();